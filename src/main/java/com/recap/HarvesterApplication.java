package com.recap;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HarvesterApplication {

	public static void main(String[] args) {
		SpringApplication.run(HarvesterApplication.class, args);
	}
}
